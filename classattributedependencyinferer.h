/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef CLASSATTRIBUTEDEPENDENCYINFERER_H
#define CLASSATTRIBUTEDEPENDENCYINFERER_H

#include <QtCore/QMultiHash>

namespace StructuredSymbolExtractor {
class Folder;
class Namespace;
class StructuredSymbolExtractor;
}

namespace SyntacticClustering {

class ClassAttributeDependencyInferer
{
public:
    ClassAttributeDependencyInferer();

    void inferDependencies(StructuredSymbolExtractor::StructuredSymbolExtractor *extractor);

    QMultiHash<StructuredSymbolExtractor::Folder *, StructuredSymbolExtractor::Folder *> folderDependencies();
    QMultiHash<StructuredSymbolExtractor::Namespace *, StructuredSymbolExtractor::Namespace *> namespaceDependencies();

private:
    QMultiHash<StructuredSymbolExtractor::Folder *, StructuredSymbolExtractor::Folder *> m_folderDependencies;
    QMultiHash<StructuredSymbolExtractor::Namespace *, StructuredSymbolExtractor::Namespace *> m_namespaceDependencies;

    StructuredSymbolExtractor::Namespace *namezpace;
};

} // namespace SyntacticClustering

#endif // CLASSATTRIBUTEDEPENDENCYINFERER_H
