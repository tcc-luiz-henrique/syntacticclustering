/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "dependencyinferer.h"

#include <structuredsymbolextractor/structuredsymbolextractor.h>
#include <structuredsymbolextractor/folder.h>
#include <structuredsymbolextractor/namespace.h>

#include "classattributedependencyinferer.h"
#include "functionargumentdependencyinferer.h"

#include <QtCore/QDebug>

namespace SyntacticClustering {

DependencyInferer *DependencyInferer::m_instance = 0;

void DependencyInferer::inferDependencies(StructuredSymbolExtractor::StructuredSymbolExtractor *extractor)
{
    m_extractor = extractor;
    m_classAttributeDependencyInferer = new ClassAttributeDependencyInferer;

    m_classAttributeDependencyInferer->inferDependencies(extractor);
    m_functionArgumentDependencyInferer->inferDependencies(extractor);

    qDebug() << "*************** Namespace dependencies";

    foreach (StructuredSymbolExtractor::Namespace *namezpaceA, m_classAttributeDependencyInferer->namespaceDependencies().uniqueKeys()) {

        foreach (StructuredSymbolExtractor::Namespace *namezpaceB, m_classAttributeDependencyInferer->namespaceDependencies().values(namezpaceA)) {
            qDebug() << namezpaceA->name() << "depends on" << namezpaceB->name();
        }
    }

    qDebug() << "*************** Folder dependencies";

    foreach (StructuredSymbolExtractor::Folder *folderA, m_classAttributeDependencyInferer->folderDependencies().uniqueKeys()) {

        foreach (StructuredSymbolExtractor::Folder *folderB, m_classAttributeDependencyInferer->folderDependencies().values(folderA)) {
            qDebug() << folderA->name() << "depends on" << folderB->name();
        }
    }

    // create classes for get class attribute and function argument dependencies
    // store dependencies in datastructures in this class
}

DependencyInferer *DependencyInferer::instance()
{
    if (!m_instance)
        m_instance = new DependencyInferer;
    return m_instance;
}

DependencyInferer::DependencyInferer(QObject *parent)
    : QObject(parent)
{
}

} // namespace SyntacticClustering
