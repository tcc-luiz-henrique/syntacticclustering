/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#ifndef SYNTACTICCLUSTERING_H
#define SYNTACTICCLUSTERING_H

#include "syntacticclustering_global.h"

#include <extensionsystem/iplugin.h>

namespace SyntacticClustering {

class DependencyInferer;

class SyntacticClusteringPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.liveblue.SyntacticClusteringPlugin" FILE "SyntacticClustering.json")

public:
    SyntacticClusteringPlugin();
    ~SyntacticClusteringPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

    DependencyInferer *dependencyInferer() const;

private:
    DependencyInferer *m_dependencyInferer;
};

} // namespace SyntacticClustering

#endif // SYNTACTICCLUSTERING_H

