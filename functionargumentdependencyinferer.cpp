/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "functionargumentdependencyinferer.h"

#include <structuredsymbolextractor/structuredsymbolextractor.h>
#include <structuredsymbolextractor/namespace.h>
#include <structuredsymbolextractor/folder.h>
#include <structuredsymbolextractor/clazz.h>
#include <structuredsymbolextractor/function.h>
#include <structuredsymbolextractor/attribute.h>
#include <structuredsymbolextractor/utilities.h>

namespace SyntacticClustering {

FunctionArgumentDependencyInferer::FunctionArgumentDependencyInferer()
{
}

void FunctionArgumentDependencyInferer::inferDependencies(StructuredSymbolExtractor::StructuredSymbolExtractor *extractor)
{
//    qDebug() << "*********** infering dependencies by function argument";

    // for each function from all classes
    foreach (StructuredSymbolExtractor::Function *function, extractor->functionsFromClasses().values()) {

//        qDebug() << "   inspecting function >>" << function->visibility() << function->returnType() << function->name() << "(" << function->arguments() << ")";

        // for each argument from current function
        foreach (StructuredSymbolExtractor::Function::Argument argument, function->arguments()) {

            // for each class B from each attribute from each method from each class from project
            foreach (StructuredSymbolExtractor::Clazz *inspectingClazz, extractor->classesFromFiles().values()) {

//                qDebug() << "       " << argument.first << "==" << inspectingClazz->name();

                // if class A == clas B then store dependencies from namespaces
                if (argument.first == inspectingClazz->name()) {

//                    qDebug() << StructuredSymbolExtractor::ClazzUtilities::generateCompleteName(argument.first)
//                             << "=="
//                             << StructuredSymbolExtractor::ClazzUtilities::generateCompleteName(inspectingClazz);

//                    qDebug() << argument.first
//                             << "=="
//                             << StructuredSymbolExtractor::ClazzUtilities::generateCompleteName(inspectingClazz);

                    // fill dependency maps according to dependencies found

                    // pegar NAMESPACE da classe tipo do argumento para armazenar. pegar também a pasta
                    m_namespaceDependencies.insertMulti(inspectingClazz->enclosingNamespace(),
                                                        extractor->namespaceFromClass(argument.first));
                    m_folderDependencies.insertMulti(extractor->folderFromClazz(inspectingClazz),
                                                     extractor->folderFromClazz(extractor->findClazzByName(argument.first)));
                }
            }
        }
    }
}

QMultiHash<StructuredSymbolExtractor::Folder *, StructuredSymbolExtractor::Folder *> FunctionArgumentDependencyInferer::folderDependencies()
{
    return m_folderDependencies;
}

QMultiHash<StructuredSymbolExtractor::Namespace *, StructuredSymbolExtractor::Namespace *> FunctionArgumentDependencyInferer::namespaceDependencies()
{
    return m_namespaceDependencies;
}

} // namespace SyntacticClustering
