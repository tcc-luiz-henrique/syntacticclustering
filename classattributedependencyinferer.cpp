/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "classattributedependencyinferer.h"

#include <structuredsymbolextractor/structuredsymbolextractor.h>
#include <structuredsymbolextractor/namespace.h>
#include <structuredsymbolextractor/folder.h>
#include <structuredsymbolextractor/clazz.h>
#include <structuredsymbolextractor/function.h>
#include <structuredsymbolextractor/attribute.h>

namespace SyntacticClustering {

ClassAttributeDependencyInferer::ClassAttributeDependencyInferer()
{
}

void ClassAttributeDependencyInferer::inferDependencies(StructuredSymbolExtractor::StructuredSymbolExtractor *extractor)
{
//    qDebug() << "*********** infering dependencies by class attribute";

    // for each class A in project
    foreach (StructuredSymbolExtractor::Clazz *clazzA, extractor->classesFromFiles().values()) {

        QString clazzAName = clazzA->name();

//        qDebug() << "   " << clazzAName;

        // for each class B from each attribute from each method from each class from project
        foreach (StructuredSymbolExtractor::Clazz *inspectingClazz, extractor->classesFromFiles().values()) {

            foreach (StructuredSymbolExtractor::Attribute *attribute, extractor->attributesFromClasses().values(inspectingClazz)) {

//                qDebug() << ")(*)(*)" << attribute->typeClazz()->name();

                QString attributeTypeName = attribute->typeName();

//                qDebug() << "       " << attributeTypeName;

                // if class A == clas B then store dependencies from namespaces
                if (attributeTypeName == clazzAName) {

                    // fill dependency maps according to dependencies found
                    m_namespaceDependencies.insertMulti(clazzA->enclosingNamespace(), attribute->typeClazz()->enclosingNamespace());
                    m_folderDependencies.insertMulti(extractor->folderFromClazz(clazzA), extractor->folderFromClazz(attribute->typeClazz()));
                }
            }
        }
    }

}

QMultiHash<StructuredSymbolExtractor::Folder *, StructuredSymbolExtractor::Folder *> ClassAttributeDependencyInferer::folderDependencies()
{
    return m_folderDependencies;
}

QMultiHash<StructuredSymbolExtractor::Namespace *, StructuredSymbolExtractor::Namespace *> ClassAttributeDependencyInferer::namespaceDependencies()
{
    return m_namespaceDependencies;
}

} // namespace SyntacticClustering
