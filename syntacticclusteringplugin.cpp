/********************************************************************************
 *   Copyright (C) 2012 by Sandro Andrade <sandroandrade@kde.org>               *
 *                 and Luiz Henrique dos Anjos <lhenriqueanjos@ifba.edu.br>     *
 *                                                                              *
 *   This program is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the                              *
 *   Free Software Foundation, Inc.,                                            *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .             *
 *******************************************************************************/

#include "syntacticclusteringplugin.h"
#include "dependencyinferer.h"

#include <structuredsymbolextractor/structuredsymbolextractor.h>

#include <coreplugin/icore.h>
#include <coreplugin/icontext.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/actionmanager/command.h>
#include <coreplugin/actionmanager/actioncontainer.h>
#include <coreplugin/coreconstants.h>

#include <QAction>
#include <QMessageBox>
#include <QMainWindow>
#include <QMenu>
#include <QDebug>

#include <QtPlugin>

namespace SyntacticClustering {

SyntacticClusteringPlugin::SyntacticClusteringPlugin() :
    m_dependencyInferer(DependencyInferer::instance())
{
}

SyntacticClusteringPlugin::~SyntacticClusteringPlugin()
{
}

bool SyntacticClusteringPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)

    connect(StructuredSymbolExtractor::StructuredSymbolExtractor::instance(),
            SIGNAL(dataStructuresChanged(::StructuredSymbolExtractor::StructuredSymbolExtractor*)),
            m_dependencyInferer,
            SLOT(inferDependencies(::StructuredSymbolExtractor::StructuredSymbolExtractor*)));

    return true;
}

void SyntacticClusteringPlugin::extensionsInitialized()
{
}

ExtensionSystem::IPlugin::ShutdownFlag SyntacticClusteringPlugin::aboutToShutdown()
{
    return SynchronousShutdown;
}

} // namespace SyntacticClustering

Q_EXPORT_PLUGIN2(SyntacticClustering, SyntacticClustering::SyntacticClusteringPlugin)
